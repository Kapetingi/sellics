package com.sellics.estimater.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Keyword should be not empty or don't content any restricted symbols")
public class BadKeywordException extends Exception {


    static final long serialVersionUID = -3387516923224229948L;

    public BadKeywordException(String message)
    {
        super(message);
    }
}
