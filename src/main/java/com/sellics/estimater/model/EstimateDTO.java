package com.sellics.estimater.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EstimateDTO {

    private String keyword;

    private float score;



}
