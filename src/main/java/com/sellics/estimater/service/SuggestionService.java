package com.sellics.estimater.service;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface SuggestionService {

    /**
     * Return list of suggestions for given prefix
     * @param prefix query prefix
     * @return list of suggestions
     */
    List<String> getSuggestions(@NotNull String prefix);
}
