package com.sellics.estimater.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AmazonEstimateService implements EstimateService {


    private final SuggestionService suggestionService;

    @Autowired
    public AmazonEstimateService(SuggestionService suggestionService) {
        this.suggestionService = suggestionService;
    }


    /**
     * If we assign the max score to keyword what was found by first letter of it
     * and 0 for keyword what was not found even we send all keyword as prefix
     * it should looks like ~ 1/length(prefix). In case when we have not exact mach query
     * for example keyword iphone and no query mach exact only iPhone we consider that it will be enough to find it in suggestion
     *
     * @param keyword
     * @return
     */
    @Override
    public float estimate(String keyword) {
        Objects.requireNonNull(keyword);

        float score = 0;
        int step = 1;
        String normalizedKeyword = keyword.toLowerCase();
        for (int i = 1; i < keyword.length(); i++) {
            List<String> suggestions = suggestionService.getSuggestions(normalizedKeyword.substring(0, i));
            if (suggestions.contains(keyword)) {
                score = 100 / (step);
            } else {
                String[] suggestionArr = suggestions.toArray(new String[suggestions.size() + 1]);
                suggestionArr[suggestionArr.length - 1] = keyword;
                String commonPrefix = StringUtils.getCommonPrefix(suggestionArr);
                if (keyword.equals(commonPrefix)) {
                    return 100 / (step);
                }
                i = commonPrefix.length()>i?commonPrefix.length():i;
                step++;
            }

        }
        return score;
    }

}
