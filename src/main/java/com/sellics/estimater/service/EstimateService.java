package com.sellics.estimater.service;

import javax.validation.constraints.NotNull;


public interface EstimateService {

    /**
     * Return estimation how often this query is coming to amazon site
     * @param keyword - query for estimation
     * @return int estimation query frequency
     */
    float estimate(@NotNull String keyword);
}
