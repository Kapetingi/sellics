package com.sellics.estimater.service;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
public class AmazonSuggestionsService implements SuggestionService {

    private final String DEFAULT_LAN = "de_DE";

    @Override
    public List<String> getSuggestions(String prefix) {
        Objects.requireNonNull(prefix);

        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "https://completion.amazon.co.uk/api/2017/suggestions?" +
                "&mid=A1PA6795UKMFR9" +
                "&alias=aps" +
                "&lop=" + DEFAULT_LAN +
                "&ks=undefined" +
                "&prefix=" + prefix +
                "&event=onFocusEmptySearchTerm" +
                "&suggestion-type=KEYWORD" +
                "&suggestion-type=WIDGET" +
                "&_=1563223246648";
        ResponseEntity<String> json
                = restTemplate.getForEntity(fooResourceUrl, String.class);
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json.getBody());

        return JsonPath.read(document, "$.suggestions[*].value");
    }
}
