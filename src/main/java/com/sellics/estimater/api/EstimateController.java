package com.sellics.estimater.api;

import com.sellics.estimater.exception.BadKeywordException;
import com.sellics.estimater.model.EstimateDTO;
import com.sellics.estimater.service.EstimateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EstimateController {

    private EstimateService estimateService;

    @Autowired
    public EstimateController(EstimateService estimateService) {
        this.estimateService = estimateService;
    }

    @GetMapping("estimate")
    public EstimateDTO estimate(String keyword) throws BadKeywordException {
        if(keyword == null || keyword.isEmpty()){
            throw new BadKeywordException("Keyword should be not empty or don't content any restricted symbols");
        }
        EstimateDTO result = new EstimateDTO();
        result.setKeyword(keyword);
        result.setScore(estimateService.estimate(keyword));
        return result;
    }
}
